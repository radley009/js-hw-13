const img = document.querySelectorAll('.image-to-show');
const wrapper = document.querySelector('.images-wrapper');
const timerElement = document.getElementById("timer");
let currentIndex = 0;
let timeoutId;
let intervalId;
let countdown = 3000;

function timerDisplay() {
    const seconds = Math.floor(countdown / 1000);
    const milliseconds = countdown % 1000;

    timerElement.textContent = `Час до наступної картинки: ${seconds} с ${milliseconds} мс`;
}
function show() {
    img.forEach((item, index) => {
        if(index === currentIndex) {
            item.classList.add('active');
        } else {
            item.classList.remove('active');
        }
    });
    currentIndex = (currentIndex + 1) % img.length;


    countdown = 3000;
    intervalId = setInterval(() => {
        countdown -= 10;
        timerDisplay();
        if (countdown <= 0) {
            clearInterval(intervalId);
            clearTimeout(timeoutId);
            timeoutId = setTimeout(show, 0);
        }
    }, 10);


    if(!document.querySelector('.btn__reset')) {
        const btnReset = document.createElement("button");
        btnReset.classList.add('btn__reset');
        wrapper.after(btnReset);
        btnReset.textContent = "Відновити показ";
    }

    if(!document.querySelector('.btn')) {
        const btnStop = document.createElement("button");
        btnStop.classList.add('btn');
        wrapper.after(btnStop);
        btnStop.textContent = "Припинити";
    }

}
show();

const stopButton = document.querySelector('.btn');
stopButton.addEventListener('click', () => {
    clearTimeout(timeoutId);
    clearInterval(intervalId);
});


const resumeButton = document.querySelector('.btn__reset');
resumeButton.addEventListener('click', () => {
    clearTimeout(timeoutId);
    clearInterval(intervalId);
    show();
});


